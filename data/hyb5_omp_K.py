#-*- coding: utf-8 -*-
import pandas as pd
from pandas import Series,DataFrame
import seaborn as sns
import matplotlib.pyplot as plt 
from matplotlib.font_manager import FontProperties 
import subprocess
#subprocess.call(['dot', '-Tpdf', 'tree.dot', '-o' 'tree.pdf'])
import pydot
from scipy import stats as scistats
import string
import numpy as np 
 

font2 = {'family': 'Times New Roman',  
        'color':  'black',  
        'weight': 'normal',  
        'size': 15,  
        }  

def df():
    with open('cage12_hyb5_testk_near_ave_his.dat') as f: 
        lines = f.readlines()

    index=[]
    gf_data=[]
    for line in lines:
        if 'GFlops' in line:
            st=line.split(',')[3].split('=')[1]
            if st not in index:
                index.append(int(st))
                st=line.split('GFlops')[1][3:]
                gf_data.append(float(st))
    

        
    return index,gf_data


   
if __name__ == '__main__':
    x,y=df()


    plt.figure()  
    plt.plot(x,y,'o-',label='HYB5')
    plt.plot([0,40],[25,25],c='r',label='SELL')
    plt.plot([0,40],[15.04,15.04],c='y',label='CSR5')
    xx=30
    yy=23
    plt.text(xx, yy, r'$histogram$', fontdict=font2)
    plt.annotate('',xy=(20,25.7),xytext=(xx,yy),arrowprops=dict(arrowstyle="->",connectionstyle="arc3"))
    xx=0
    yy=20
    plt.text(xx, yy, r'$traditional$', fontdict=font2)
    plt.annotate('',xy=(15,20.63),xytext=(xx+10,yy+0.1),arrowprops=dict(arrowstyle="->",connectionstyle="arc3"))
    #xx=60
    #yy=5.7
    #plt.text(xx, yy, r'$A$', fontdict=font2)
    #plt.annotate('',xy=(65,5.53),xytext=(xx+2,yy),arrowprops=dict(arrowstyle="->",connectionstyle="arc3"))
    
    plt.ylabel('Gflops',size=20)
    plt.xlabel('K', size=17)
    plt.legend(loc='best')
    leg = plt.gca().get_legend()
    ltext  = leg.get_texts()
    plt.setp(ltext, fontsize=15)
    
    plt.subplots_adjust(bottom=0,top=0.6)

    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.subplots_adjust(bottom=0.3,top=0.8)    
    plt.savefig('K_image.pdf',dpi=400,bbox_inches='tight')
    #plt.show()
    '''
    plt.title('ft2000p_thread64(mtx orderd by nnz)')
    plt.ylabel('GFlops')
    #plt.xlabel('mtx_num')
    plt.legend(loc='upper left',ncol=1)
    plt.xlim([0,1000])
    plt.ylim([0,60])
    plt.savefig('scatter.pdf',dpi=400,bbox_inches='tight')
    '''


    
