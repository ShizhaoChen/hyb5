import string
import numpy as np 
from pandas import Series,DataFrame
import pandas as pd
import matplotlib.pyplot as plt
from pylab import *
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
#subprocess.call(['dot', '-Tpdf', 'tree.dot', '-o' 'tree.pdf'])
import pydot
from scipy import stats as scistats



df_tmp=pd.read_csv('hyb5_csr5_sell_speedup_forPPT.csv')#, index_col=0)#[0:15]

df=df_tmp[21:30]
#df.to_csv('tmp.csv')
csr5=np.array(df['gflops_csr5'])#.reshape(-1,1)
sell=np.array(df['gflops_sell'])#.reshape(-1,1)
hyb5=np.array(df['GFlops'])#.reshape(-1,1)
df_bar=pd.DataFrame({'csr5':csr5, 'sell':sell, 'hyb5':hyb5}, index=df["Matrix"])
#plt.scatter(df["scatter_x"], df["gflops_csr5"], color='black', label='csr5')
#plt.scatter(df["scatter_x"], df["gflops_sell"], color='blue', label='sell')
#plt.scatter(df["scatter_x"], df["GFlops"], color='red', label='hyb5')
#width=0.3
#x = np.arange(14)
ax=df_bar.plot.bar(rot=30)
plt.ylabel('GFlops')
#plt.bar(x, df["gflops_csr5"], label="csr5", color='b', width='.3')
#plt.bar(x+0.3, df["gflops_sell"], label="sell", color='green', width='.3')
#plt.bar(x+0.6, df["GFlops"], label="hyb5", color='red', width='.3')
plt.show()