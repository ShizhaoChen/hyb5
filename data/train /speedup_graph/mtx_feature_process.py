import string
import numpy as np
from pandas import Series,DataFrame
import pandas as pd
import math
from sklearn.preprocessing import StandardScaler
mtx_csr5=[]
gflops_csr5=[]

mtx_sell=[]
gflops_sell=[]

with open("csr5_avx512.dat") as f1: 
    lines = f1.readlines()
    
for line in lines:
    if 'CSR5-based SpMV time' in line:
        st=line.split('mtx')[1].strip(string.punctuation)
        mtx_csr5.append(st)
            
        st=line.split('GFlops')[1].split(' ')[2]#.strip(string.punctuation)#split()[2]
        #tmp=st[3:]#filter(str.isdigit, st)
        gflops_csr5.append(st)

data_csr5={'mtx':mtx_csr5, 'gflops_csr5':gflops_csr5}
df_csr5=DataFrame(data_csr5)

with open("sell_avx512.dat") as f1:#"hyb5_avx512_bestGFlops_3K.dat") as f1: 
    lines = f1.readlines()

for line in lines:
    if 'cpu sequential min_time' in line:
        st=line.split('mtx')[2].strip(string.punctuation)
        mtx_sell.append(st)
        
        st=line.split('GFlops')[1].split(' ')[2]
        gflops_sell.append(float(st))

df_sell=DataFrame({'mtx':mtx_sell, 'gflops_sell':gflops_sell})
df_csr5.to_csv('csr5_avx512.csv')
df_sell.to_csv('sell_avx512.csv')

df_hyb5=pd.read_csv('hyb5_feature.csv', index_col=0)

df_tmp=pd.merge(df_csr5, df_sell, on='mtx', how='inner')
df_all=pd.merge(df_tmp, df_hyb5, on='mtx', how='inner')

df_all.to_csv('hyb5_csr5_sell.csv')

'''
with open("Variance.dat") as f1: 
    lines = f1.readlines()

for line in lines:
    if 'variance' in line:
        st=line.split('mtx')[2].strip(string.punctuation)
        index.append(st)
        st=line.split('variance')[1].split(',')[0][3:]
        var.append(float(st))

df_var=DataFrame({'mtx':index,'var':var})

df_tmp=pd.merge(df_feature, df_var, on='mtx', how='inner')

df_all=pd.merge(df_tmp, df_hyb5, on='mtx', how='inner')

df_all.to_csv('hyb5_feature.csv')

for i in range(1,df_all.iloc[0,:].size):
    for j in range(0,df_all.iloc[:,0].size):
        if float(df_all.iloc[j,i]) == 0:
            df_all.iloc[j,i] = df_all.iloc[j,i]
        else:
            #print(df_all.iloc[j,i])
            tmp=math.log10(float(df_all.iloc[j,i]))#目标特征也需要归一化：preprocessing.standardscaler()
            df_all.iloc[j,i]=tmp#特征值筛选：因子分析、PCA主成分析

df_all.to_csv('hyb5_feature_log.csv')

df_all_sam=df_all.sample(frac=1)

df_log=df_all_sam[0:800]# ft:720  knl :760
df_log.to_csv('feature_log_train.csv')
df_log=df_all_sam[800:]
df_log.to_csv('feature_log_test.csv')   

hyb5_k_array_train = np.array(df_all['k_best'])
sumsize=hyb5_k_array_train.size

hyb5_k_array_train = hyb5_k_array_train.reshape(sumsize,1)
n_rows_array=np.array(df_all['n_rows']).reshape(sumsize,1)
n_cols_array=np.array(df_all['n_cols']).reshape(sumsize,1)
nnz_tot_array=np.array(df_all['nnz_tot']).reshape(sumsize,1)
nnz_frac_array=np.array(df_all['nnz_frac']).reshape(sumsize,1)
nnz_min_array=np.array(df_all['nnz_min']).reshape(sumsize,1)
nnz_max_array=np.array(df_all['nnz_max']).reshape(sumsize,1)
nnz_mu_array=np.array(df_all['nnz_mu']).reshape(sumsize,1)
nnz_sig_array=np.array(df_all['nnz_sig']).reshape(sumsize,1)
var_array=np.array(df_all['var']).reshape(sumsize,1)
k_ave_array=np.array(df_all['k_average']).reshape(sumsize,1)
k_his_array=np.array(df_all['k_histogram']).reshape(sumsize,1)

feature_log_all = np.hstack((n_rows_array,n_cols_array,nnz_tot_array,nnz_frac_array,nnz_min_array,nnz_max_array,nnz_mu_array,nnz_sig_array,var_array,k_ave_array,k_his_array,hyb5_k_array_train))

standard = StandardScaler()
feature_log_scaler_all = standard.fit_transform(feature_log_all)

df_scaler_all = pd.DataFrame(feature_log_scaler_all)

df_scaler_all.columns=['n_rows','n_cols','nnz_tot','nnz_frac','nnz_min','nnz_max','nnz_mu','nnz_sig','var','k_average','k_histogram','k_best']
#df_scaler_all.rename(columns={'0':'n_rows','1':'n_cols','2':'nnz_tot','3':'nnz_frac','4':'nnz_min','5':'nnz_max','6':'nnz_mu','7':'nnz_sig','8':'var','9':'k_average','10':'k_histogram','11':'k_best'}, inplace=True)

df_scaler_all.to_csv('feature_log_scaler_all.csv')

df_scaler_all_sam=df_scaler_all.sample(frac=1)
df_scaler_all=df_scaler_all_sam[0:800]
df_scaler_all.to_csv('feature_log_scaler_train.csv')
df_scaler_all=df_scaler_all_sam[800:]
df_scaler_all.to_csv('feature_log_scaler_test.csv')

'''
