import string
import numpy as np
from pandas import Series,DataFrame
import pandas as pd
import os

path="/Volumes/chenshizhao1T/MAC/Download/ssget/mtx"
#path="E:/mtx"
mt_n=[]
n_r=[]
n_c=[]
nnz_t=[]
nnz_f=[]

nnz_mi=[]
nnz_ma=[]
nnz_mu=[]
nnz_sig=[]

def process(filename):
    print filename
    with open(path+"/"+filename) as f1: 
        lines = f1.readlines()
    
    flag=0

    if "symmetric" in lines[0]:
        flag=1
    

        
    for line in lines:
        if "%" in line:
            continue
        else:
            star=line
            break
    try:
        n_rows=int(star.split(' ')[0])
        n_cols=int(star.split(' ')[1])
        nnz_tmp=int(star.split(' ')[2])
    except:
        return
    else:
        zhouxian=0
        
        tmp = [0]* (n_rows+1) #array start from 0
        
        for line in lines:
            if "%" in line:
                continue
            elif line == star:
                continue
            elif line == '\n':
                continue
            else:
                #print line.split(' ')[0]
                try:
                    a=int(line.split(' ')[0])
                    b=int(line.split(' ')[1])
                except:
                    return
                else:
                    if flag == 0:
                        tmp[a]+=1
                    else:
                        if a == b:
                            zhouxian+=1
                            tmp[a]+=1
                        else:
                            tmp[a]+=1
                            tmp[b]+=1


        if flag == 0:
            nnz_tot=nnz_tmp
        else:
            nnz_tot=nnz_tmp*2 - zhouxian

        nnz_frac=float(nnz_tot)/(n_rows*n_cols)
        
        np_tmp=np.array(tmp)
        np_tmp=np_tmp[1:]    #delete  np_tmp[0]

        ss=filename.split('mtx')[0].strip(string.punctuation)
        
        mt_n.append(ss)
        n_r.append(n_rows)
        n_c.append(n_cols)
        nnz_t.append(nnz_tot)
        nnz_f.append(nnz_frac)
        nnz_mi.append(np_tmp.min())
        nnz_ma.append(np_tmp.max())
        nnz_mu.append(np_tmp.mean())
        nnz_sig.append(np_tmp.std())

        print flag
        
        re= np.argmax(np_tmp)
        print re
    
    
    '''  

    print sys.argv[1]+": n_rows = "+str(n_rows)+", n_cols = "+str(n_cols)+",
    nnz_tot = "+str(nnz_tot)+", nnz_frac = "+str(nnz_frac)+",
    nnz_min = "+str(np_tmp.min())+",
    nnz_max = "+str(np_tmp.max())+", nnz_mu"+str(np_tmp.mean())+",
    nnz_sig = "+str(np_tmp.std())

    '''
      


if __name__ == '__main__':
    files= os.listdir(path)
    for fil in files:
        process(fil)
    data={'n_rows':n_r,'n_rols':n_c,'nnz_tot':nnz_t,'nnz_frac':nnz_f,'nnz_min':nnz_mi,'nnz_max':nnz_ma,'nnz_mu':nnz_mu,'nnz_sig':nnz_sig}
    frame=DataFrame(data,index=mt_n)

    frame.to_csv('featuredata.csv')
    