import string
import numpy as np 
from pandas import Series,DataFrame
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from pylab import *
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from sklearn import tree
from sklearn import cross_validation
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import cross_val_predict
from sklearn.preprocessing import StandardScaler

import subprocess
#subprocess.call(['dot', '-Tpdf', 'tree.dot', '-o' 'tree.pdf'])
import pydot
from scipy import stats as scistats





    
if __name__ == '__main__':

    frame=pd.read_csv('hyb5_feature.csv',index_col=0)
    
    hyb5_k_array=np.array(frame['k_best'])
    sumsize=hyb5_k_array.size

    n_rows_array=np.array(frame['n_rows']).reshape(sumsize,1)
    n_cols_array=np.array(frame['n_cols']).reshape(sumsize,1)
    nnz_tot_array=np.array(frame['nnz_tot']).reshape(sumsize,1)
    nnz_frac_array=np.array(frame['nnz_frac']).reshape(sumsize,1)
    nnz_min_array=np.array(frame['nnz_min']).reshape(sumsize,1)
    nnz_max_array=np.array(frame['nnz_max']).reshape(sumsize,1)
    nnz_mu_array=np.array(frame['nnz_mu']).reshape(sumsize,1)
    nnz_sig_array=np.array(frame['nnz_sig']).reshape(sumsize,1)
    var_array=np.array(frame['var']).reshape(sumsize,1)
    k_ave_array=np.array(frame['k_average']).reshape(sumsize,1)
    k_his_array=np.array(frame['k_histogram']).reshape(sumsize,1)

    feature_hyb5 = np.hstack((n_rows_array,n_cols_array,nnz_tot_array,nnz_frac_array,nnz_max_array,nnz_min_array,nnz_max_array,nnz_mu_array,nnz_sig_array,var_array,k_ave_array,k_histogram))
        
    knn = DecisionTreeRegressor(max_leaf_nodes=10)
    # ft max_leaf_nodes=10
    # knl max_leaf_nodes=10,min_weight_fraction_leaf=0.15
    
    scores = cross_val_score(knn, feature_hyb5, hyb5_k_array, cv = 10, scoring = 'accuracy')
    print (scores)
    pre_result=cross_val_predict(knn, feature_hyb5, hyb5_k_array, cv = 10)
    
    '''
    rate=[]
    for i in range(0,sumsize):
        #print pre_result[i],format_array[i]
        if format_array[i]==1:
            real=frame.ix[i]['csr']
        elif format_array[i]==2:
            real=frame.ix[i]['csr5']
        elif format_array[i]==3:
            real=frame.ix[i]['ell']          
        elif format_array[i]==4:
            real=frame.ix[i]['hyb']
        elif format_array[i]==5:
            real=frame.ix[i]['sell']           

        if pre_result[i]==1:
            pre=frame.ix[i]['csr']
        elif pre_result[i]==2:
            pre=frame.ix[i]['csr5']
        elif pre_result[i]==3:
            pre=frame.ix[i]['ell']          
        elif pre_result[i]==4:
            pre=frame.ix[i]['hyb']
        elif pre_result[i]==5:
            pre=frame.ix[i]['sell']

        rate.append(pre/real)

    f_rate=scistats.gmean(rate)
    print f_rate
'''
    # Fit regression model123456
    clf = tree.DecisionTreeRegressor(max_leaf_nodes=10)#max_leaf_nodes=10, splitter='random')
    clf = clf.fit(feature_hyb5, hyb5_k_array)

    print (clf.feature_importances_)

'''
    frame=pd.read_csv('final_feature_test_ft.csv',index_col=0)
    
    format_array=np.array(frame['best'])
    sumsize=len(frame)

    n_rows_array=np.array(frame['n_rows']).reshape(sumsize,1)
    n_cols_array=np.array(frame['n_cols']).reshape(sumsize,1)
    nnz_tot_array=np.array(frame['nnz_tot']).reshape(sumsize,1)
    nnz_frac_array=np.array(frame['nnz_frac']).reshape(sumsize,1)
    nnz_min_array=np.array(frame['nnz_min']).reshape(sumsize,1)
    nnz_max_array=np.array(frame['nnz_max']).reshape(sumsize,1)
    nnz_mu_array=np.array(frame['nnz_mu']).reshape(sumsize,1)
    nnz_sig_array=np.array(frame['nnz_sig']).reshape(sumsize,1)
    var_array=np.array(frame['var']).reshape(sumsize,1)

    feature_test = np.hstack((n_rows_array,nnz_tot_array,nnz_max_array))
    
    result= clf.predict(feature_test)
    print result
    score=clf.score(feature_test,format_array)
    print score
'''
'''
    rate=[]
    for i in range(0,sumsize):
        #print result[i],format_array[i]
        if format_array[i]==1:
            real=frame.ix[i]['csr']
        elif format_array[i]==2:
            real=frame.ix[i]['csr5']
        elif format_array[i]==3:
            real=frame.ix[i]['ell']          
        elif format_array[i]==4:
            real=frame.ix[i]['hyb']
        elif format_array[i]==5:
            real=frame.ix[i]['sell']           

        if result[i]==1:
            pre=frame.ix[i]['csr']
        elif result[i]==2:
            pre=frame.ix[i]['csr5']
        elif result[i]==3:
            pre=frame.ix[i]['ell']          
        elif result[i]==4:
            pre=frame.ix[i]['hyb']
        elif result[i]==5:
            pre=frame.ix[i]['sell']
        rate.append(pre/real)

    f_rate=scistats.gmean(rate)
    print f_rate
'''
    
    

