
#rm tmp.txt
awk 'BEGIN {count_name=0; count_gflops=0;} {if (name[count_name]!=$1) {max=0; index_=0;for(i = 0; i < count_gflops; i++){if(gflops[i] > max) {max=gflops[i]; index_=i;}}print name[count_name] " " max " " k[index_] " k_histogram = " k_histogram[index_] " k_average = " k_average[index_] >> "tmp_2.txt"; count_name++;name[count_name] = $1; count_gflops=0; } else{ gflops[count_gflops]=$2; k[count_gflops]=$6; k_histogram[count_gflops]=$9; k_average[count_gflops]=$12; count_gflops++; } }' tmp.txt

#awk '{if ($22~".mtx") print $22 " " $15 " GFlops, K = " $25 " K_histogram = " $34 " K_average = " $37 >>"tmp.txt";}' hyb5_avx512_testk_near_ave_his_raw.dat


#HYB5_sellAndcsr5 : cpu sequential min_time = 0.062 ms. Bandwidth = 36.7855 GB/s. GFlops = 3.47535 GFlops, NNZ = 107736, mtx_name = ../../mtx/vsp_p0291_seymourl_iiasa.mtx, hyb5_k = 21


#awk -F: 'BEGIN {count=0;} {name[count] = $1;count++;}; END{for (i = 0; i < NR; i++) print i, name[i]}'