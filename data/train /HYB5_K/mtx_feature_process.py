import string
import numpy as np
from pandas import Series,DataFrame
import pandas as pd
import math

mt_n=[]
n_r=[]
n_c=[]
nnz_t=[]
nnz_f=[]

nnz_mi=[]
nnz_ma=[]
nnz_mu=[]
nnz_sig=[]
index=[]
var=[]

k=[]
k_average=[]
k_histogram=[]
index_k=[]
gflops=[]


with open("nnz_feature.dat") as f1: 
    lines = f1.readlines()
    
for line in lines:
    if 'mtx' in line:
        st=line.split('mtx')[1].strip(string.punctuation)
        mt_n.append(st)
            
        st=line.split('n_rows')[1].split(',')[0]#.strip(string.punctuation)#split()[2]
        tmp=st[3:]#filter(str.isdigit, st)
        n_r.append(int(tmp))

        st=line.split('n_cols')[1].split(',')[0]
        tmp=st[3:]#tmp=filter(str.isdigit, st)
        n_c.append(int(tmp))

        st=line.split('nnz_tot')[1].split(',')[0]
        tmp=st[3:]
        nnz_t.append(int(tmp))

        st=line.split('nnz_frac')[1].split(',')[0]
        tmp=st[3:]
        nnz_f.append(float(tmp))

        st=line.split('nnz_min')[1].split(',')[0]
        tmp=st[3:]#filter(str.isdigit, st)
        nnz_mi.append(int(tmp))

        st=line.split('nnz_max')[1].split(',')[0]
        tmp=st[3:]#filter(str.isdigit, st)
        nnz_ma.append(int(tmp))

        st=line.split('nnz_mu')[1].split(',')[0]
        tmp=st[3:]
        nnz_mu.append(float(tmp))

        st=line.split('nnz_sig')[1].split(',')[0]
        tmp=st[3:]
        nnz_sig.append(float(tmp))
        
data={'mtx':mt_n,'n_rows':n_r,'n_cols':n_c,'nnz_tot':nnz_t,'nnz_frac':nnz_f,'nnz_min':nnz_mi,'nnz_max':nnz_ma,'nnz_mu':nnz_mu,'nnz_sig':nnz_sig}
df_feature=DataFrame(data)

with open("hyb5_avx512_bestGFlops_3K.dat") as f1: 
    lines = f1.readlines()

for line in lines:
    if 'mtx' in line:
        st=line.split('mtx')[0].strip(string.punctuation)
        index_k.append(st)
        
        st=line.split()[1]
        gflops.append(float(st))
        
        st=line.split()[2]
        k.append(int(st))
        
        st=line.split(' ')[5]#.split('k_average')[0]
        k_histogram.append(st)
        
        st=line.split(' ')[8]
        k_average.append(st)

df_hyb5=DataFrame({'mtx':index_k, 'gflops':gflops, 'hyb5_k':k, 'k_average':k_average, 'k_histogram':k_histogram})


with open("Variance.dat") as f1: 
    lines = f1.readlines()

for line in lines:
    if 'variance' in line:
        st=line.split('mtx')[2].strip(string.punctuation)
        index.append(st)
        st=line.split('variance')[1].split(',')[0][3:]
        var.append(float(st))
        

df_var=DataFrame({'mtx':index,'var':var})

df_tmp=pd.merge(df_feature, df_var, on='mtx', how='inner')

df_all=pd.merge(df_tmp, df_hyb5, on='mtx', how='inner')
'''
print sys.argv[1]+": n_rows = "+str(n_rows)+", n_cols = "+str(n_cols)+",
nnz_tot = "+str(nnz_tot)+", nnz_frac = "+str(nnz_frac)+",
nnz_min = "+str(np_tmp.min())+",
nnz_max = "+str(np_tmp.max())+", nnz_mu"+str(np_tmp.mean())+",
nnz_sig = "+str(np_tmp.std())

'''

#obj=Series(var,index=index)

# frame['var']=obj
#frame=frame.reindex(columns=['n_rows','n_cols','nnz_tot','nnz_frac','nnz_min','nnz_max','nnz_mu','nnz_sig','var'])

#data_hyb5={'gflops':gflops, 'k':k}
#df_hyb5=DataFrame(data_hyb5,index=index_k)

#df_all=pd.merge(frame,df_hyb5,on='',how='inner')

df_all.to_csv('hyb5_feature.csv')