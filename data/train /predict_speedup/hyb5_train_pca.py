import string
import numpy as np 
from pandas import Series,DataFrame
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from pylab import *
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from sklearn import tree
from sklearn import cross_validation
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import cross_val_predict
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error
from sklearn.decomposition import PCA 
import collections

import subprocess
#subprocess.call(['dot', '-Tpdf', 'tree.dot', '-o' 'tree.pdf'])
import pydot
from scipy import stats as scistats


frame=pd.read_csv('feature_standard_average.csv', index_col=0)

k_array=np.array(frame['k'])
sumsize=k_array.size

mtx_array=np.array(frame['mtx'], str).reshape(sumsize,1)
n_rows_array=np.array(frame['n_rows']).reshape(sumsize,1)
n_cols_array=np.array(frame['n_cols']).reshape(sumsize,1)
nnz_tot_array=np.array(frame['nnz_tot']).reshape(sumsize,1)
nnz_frac_array=np.array(frame['nnz_frac']).reshape(sumsize,1)
nnz_min_array=np.array(frame['nnz_min']).reshape(sumsize,1)
nnz_max_array=np.array(frame['nnz_max']).reshape(sumsize,1)
nnz_mu_array=np.array(frame['nnz_mu']).reshape(sumsize,1)#######
nnz_sig_array=np.array(frame['nnz_sig']).reshape(sumsize,1)
var_array=np.array(frame['var']).reshape(sumsize,1)
k_ave_array=np.array(frame['k_average_old']).reshape(sumsize,1)################
k_his_array=np.array(frame['k_histogram_old']).reshape(sumsize,1)###########
gflops_array=np.array(frame['gflops_old']).reshape(sumsize,1)
speedup_array=np.array(frame['speedup']).reshape(sumsize,1)###########

k_array = k_array.reshape((-1,1))

#feature_all = np.hstack((k_array, n_rows_array, n_cols_array, nnz_tot_array, nnz_frac_array, nnz_min_array, nnz_max_array, nnz_mu_array, nnz_sig_array, var_array, k_ave_array, k_his_array, gflops_old_array, speedup))
feature_all = np.hstack((k_array, n_rows_array, n_cols_array, nnz_tot_array, nnz_frac_array, nnz_min_array, nnz_max_array, nnz_mu_array, nnz_sig_array, var_array, k_ave_array, k_his_array))

standard = StandardScaler()
feature_all_scale=standard.fit_transform(feature_all)

pca=PCA(n_components=5)
reduced_feature_all = pca.fit_transform(feature_all_scale)

df_reduced_feature_all = pd.DataFrame(reduced_feature_all)
#df_feature_all.columns=['k', 'n_rows', 'n_cols', 'nnz_tot', 'nnz_frac', 'nnz_min','nnz_max','nnz_mu','nnz_sig','var','k_average','k_histogram']
#df_feature_all.insert(loc=0, column='mtx', value=mtx_array)
df_reduced_feature_all.columns=['a', 'b', 'c', 'd', 'e']#, 'f']
df_reduced_feature_all.insert(loc=0, column='mtx', value=mtx_array)
df_reduced_feature_all.insert(loc=6, column='speedup', value=speedup_array)
df_reduced_feature_all.insert(loc=7, column='gflops', value=gflops_array)
#df_reduced_feature_train.to_csv('reduced_feature_train.csv')

df_train=df_reduced_feature_all[0:47850]
df_train.to_csv('reduced_feature_train_scale_pca.csv')
df_test=df_reduced_feature_all[47851:]
df_test.to_csv('reduced_feature_test_scale_pca.csv')

mtx_array=np.array(df_train['mtx'])#.reshape(sumsize,1)
sumsize=mtx_array.size
mtx_array=mtx_array.reshape(-1,1)
one_array=np.array(df_train['a']).reshape(sumsize,1)
two_array=np.array(df_train['b']).reshape(sumsize,1)################
three_array=np.array(df_train['c']).reshape(sumsize,1)###########
four_array=np.array(df_train['d']).reshape(sumsize,1)###########
five_array=np.array(df_train['e']).reshape(sumsize,1)###########
#six_array=np.array(df_train['f']).reshape(sumsize,1)###########
gflops_train=np.array(df_train['gflops'])
speedup_train=np.array(df_train['speedup']).reshape(-1,1)

feature_train = np.hstack((one_array, two_array, three_array, four_array, five_array))#, six_array))

df_reduced_feature_train=DataFrame(feature_train)
df_reduced_feature_train.to_csv('reduced_feature_train.csv')

#clf = DecisionTreeRegressor(max_leaf_nodes=10, splitter='random')
#clf = clf.fit(feature_train, speedup_train)

train_count=0
for i in range(1,sumsize-1):
    if mtx_array[i] != mtx_array[i+1]:
        train_count+=1
print(train_count)
#print (clf.feature_importances_)

clf_svr = svm.SVR(kernel='rbf', C=1.0, epsilon=8.19, degree=300)#, degree=3, gamma='auto')#kernel='rbf', degree=3, gamma='auto', coef0=0.0, tol=1e-3, C=1.0, epsilon=0.1, shrinking=True, cache_size=200, verbose=False, max_iter=-1)#kernel='poly')
clf_svr.fit(feature_train, speedup_train)#K=4, 0.788832, K=5,kernel:rbf,0.790532, linear:0.707282;K=6, 0.712031
#print (clf.feature_importances_)C=1:86.7, C=2:86.2, epsilon=8:87.89, epsilon=8.19:88.2792
#Freescale2
#clf_mlp = MLPRegressor(hidden_layer_sizes=(6,6,6))#,  activation='tanh', solver='adam')#, alpha=0.0001, batch_size='auto',
#clf_mlp.fit(feature_train, speedup_train)#K=4,0.699167, K=6, 0.709322
#learning_rate='constant', learning_rate_init=0.001, power_t=0.5, max_iter=5000, shuffle=True,
#random_state=1, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,
#early_stopping=False,beta_1=0.9, beta_2=0.999, epsilon=1e-08)
#print (clf.feature_importances_)

mtx_array=np.array(df_test['mtx'])#.reshape(sumsize,1)
sumsize=mtx_array.size
mtx_array=mtx_array.reshape(-1,1)
one_array=np.array(df_test['a']).reshape(sumsize,1)
two_array=np.array(df_test['b']).reshape(sumsize,1)################
three_array=np.array(df_test['c']).reshape(sumsize,1)###########speedup_test=np.array(feature_test['speedup']).reshape(sumsize,1)
four_array=np.array(df_test['d']).reshape(sumsize,1)
five_array=np.array(df_test['e']).reshape(sumsize,1)###########
#six_array=np.array(df_test['f']).reshape(sumsize,1)###########
speedup_test=np.array(df_test['speedup']).reshape(-1,1)
gflops_test=np.array(df_test['gflops']).reshape(sumsize,1)
feature_test = np.hstack((one_array, two_array, three_array, four_array, five_array))#, six_array))

#feature_test=standard.fit_transform(feature_test)
result_test=clf_svr.predict(feature_test)

df_feature_result=pd.DataFrame(feature_test)
df_feature_result.columns=['one', 'two', 'three', 'four', 'five']#, 'six']
df_feature_result.insert(loc=5, column='gflops',         value=gflops_test)
df_feature_result.insert(loc=6, column='result_speedup', value=result_test)
df_feature_result.insert(loc=7, column='speedup',        value=speedup_test)
df_feature_result.insert(loc=0, column='mtx',            value=mtx_array)
df_feature_result.to_csv('feature_result.csv')

gflops_best_real=[0]*200
gflops_tmp=[0]*100
gflops_best_predict=[0]*200
speedup_min_predict=[0]*200
speedup_tmp=[0]*100
in_mtx_counter=[0]*200
in_mtx_counter[0]=1
index=[0]*200
#in_mtx_counter[0]=1
name_count=0
index_gflops=[0]*200
index_speedup=[0]*200
mtx_no_repetition=[]

result_size=df_feature_result.iloc[:,0].size
for i in range(1,result_size-1):
    if mtx_array[i] == mtx_array[i+1]:#在一个矩阵的数据集范围内找最小speedup的索引和最大GFlops_real的索引
        in_mtx_counter[name_count]+=1
        #print(name_count)
    else:
        #print(i)
        max=0
        min=1000
        for j in range(i-in_mtx_counter[name_count], i):
            if df_feature_result.gflops[j] > max:
                max=df_feature_result.gflops[j]#gflops_tmp[j]
                index_gflops[name_count]=j
            if df_feature_result.result_speedup[j] < min:
                min=df_feature_result.result_speedup[j]#speedup_tmp[name_count]
                index_speedup[name_count]=j
        name_count+=1
        mtx_no_repetition.append(mtx_array[i])

#print(df_feature_result.result_speedup)     
#print(index_speedup)#换模型结果不变

sum_gflops_best=0
sum_gflops_best_predict=0
for i in range(0,name_count):
    sum_gflops_best         += df_feature_result.gflops[index_gflops[i]]
    sum_gflops_best_predict += df_feature_result.gflops[index_speedup[i]]
    differ = df_feature_result.gflops[index_gflops[i]] - df_feature_result.gflops[index_speedup[i]]
    print("differ = %f, mtx = %s" %(differ, mtx_no_repetition[i]))

print(sum_gflops_best_predict)
print(sum_gflops_best)
print('Predicted accuracy = %f' % (sum_gflops_best_predict/sum_gflops_best))
#print(sum_gflops_best)

    