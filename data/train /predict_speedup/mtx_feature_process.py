import string
import numpy as np
from pandas import Series,DataFrame
import pandas as pd
import math
from sklearn.preprocessing import StandardScaler

k=[]
k_average=[]
k_histogram=[]
mtx=[]
gflops=[]


mtx_old=[]
k_old=[]
gflops_old=[]
k_histogram_old=[]
gflops_average=[]
k_average_old=[]
mtx_counter=[]

with open("tmp_sed.txt") as f1:#"hyb5_avx512_bestGFlops_3K.dat") as f1: 
    lines = f1.readlines()

for line in lines:
    if 'mtx' in line:
        st=line.split('mtx')[0].strip(string.punctuation)
        mtx.append(st)
        
        st=line.split()[1]
        gflops.append(float(st))
        
        st=line.split(' ',12)[5]
        k.append(st)#int(st))
        
        st=line.split(' ',12)[8]#.split('k_average')[0]
        k_histogram.append(st)
        
        st=line.split(' ',12)[11].strip('\n')
        k_average.append(st)



mt_n=[]
n_r=[]
n_c=[]
nnz_t=[]
nnz_f=[]

nnz_mi=[]
nnz_ma=[]
nnz_mu=[]
nnz_sig=[]
var=[]

with open("nnz_feature.dat") as f1: 
    lines = f1.readlines()
    
for line in lines:
    if 'mtx' in line:
        st=line.split('mtx')[1].strip(string.punctuation)
        mt_n.append(st)
            
        st=line.split('n_rows')[1].split(',')[0]#.strip(string.punctuation)#split()[2]
        tmp=st[3:]#filter(str.isdigit, st)
        n_r.append(int(tmp))

        st=line.split('n_cols')[1].split(',')[0]
        tmp=st[3:]#tmp=filter(str.isdigit, st)
        n_c.append(int(tmp))

        st=line.split('nnz_tot')[1].split(',')[0]
        tmp=st[3:]
        nnz_t.append(int(tmp))

        st=line.split('nnz_frac')[1].split(',')[0]
        tmp=st[3:]
        nnz_f.append(float(tmp))

        st=line.split('nnz_min')[1].split(',')[0]
        tmp=st[3:]#filter(str.isdigit, st)
        nnz_mi.append(int(tmp))

        st=line.split('nnz_max')[1].split(',')[0]
        tmp=st[3:]#filter(str.isdigit, st)
        nnz_ma.append(int(tmp))

        st=line.split('nnz_mu')[1].split(',')[0]
        tmp=st[3:]
        nnz_mu.append(float(tmp))

        st=line.split('nnz_sig')[1].split(',')[0]
        tmp=st[3:]
        nnz_sig.append(float(tmp))

feature_size=len(mt_n)

mtx_var=[]

with open("Variance.dat") as f1: 
        lines = f1.readlines()
    
for line in lines:
    if 'variance' in line:
        st=line.split('mtx')[2].strip(string.punctuation)
        mtx_var.append(st)
        st=line.split('variance')[1].split(',')[0][3:]
        var.append(float(st))


#print(mt_n)

listsize=len(mtx)

print ("mtx_length=%d" %listsize)
#print(mtx)

in_mtx_counter=[0]*1100
in_mtx_counter[0]=1
index_average=0
average_yes=0
name_count=0

mt_n_standard=[]
n_r_standard=[]
n_c_standard=[]
nnz_t_standard=[]
nnz_f_standard=[]

nnz_mi_standard=[]
nnz_ma_standard=[]
nnz_mu_standard=[]
nnz_sig_standard=[]
var_standard=[]

mtx_real=[0]*999

#print(k_average)
            

for i in range(1, listsize-1):
    #print(mtx[i])
    #print(mtx[i+1])
    if mtx[i] == mtx[i+1]:#在一个矩阵的数据集范围内
        in_mtx_counter[name_count]+=1#记录一个数据集内有几行数据，记录值为真实值减1
        #print(k[i])
        #print(k_average[i])
        #print("k[i] = %s, k_average[i] = %s" %(k[i], k_average[i]))
        if k[i] == k_average[i]:#k_average中的每一个元素含有换行符
            average_yes = 1
            index_average=i
            #print("average_yes = %d" %average_yes)
            #print("k = %d, k_average = %d ,average_yes = %d" %average_yes)
    else:
       # print("%s, %s" %(mtx[i-1], mtx[i]))
        if mtx[i-1] == mtx[i]:#本数据集的最后一行，在这个时候处理上一个数据集的结果写入
            mtx_real[name_count] = mtx[i]
        #    print("average_yes = %d" %average_yes)
            #print(mtx[i])
            if average_yes==1:
                for j in range(i-in_mtx_counter[name_count], i):
                    #print(mtx[i])   
                    n_r_standard.append    (n_r    [mt_n.   index(mtx[i])])
                    n_c_standard.append    (n_c    [mt_n.   index(mtx[i])])
                    nnz_t_standard.append  (nnz_t  [mt_n.   index(mtx[i])])
                    nnz_f_standard.append  (nnz_f  [mt_n.   index(mtx[i])])
                    nnz_mi_standard.append (nnz_mi [mt_n.   index(mtx[i])])
                    nnz_ma_standard.append (nnz_ma [mt_n.   index(mtx[i])])
                    nnz_mu_standard.append (nnz_mu [mt_n.   index(mtx[i])])
                    nnz_sig_standard.append(nnz_sig[mt_n.   index(mtx[i])])
                    var_standard.append    (var    [mtx_var.index(mtx[i])])
                    gflops_average.append(gflops[index_average])#.insert(j, gflops[index_average])#[j].append(gflops[j])
                    k_average_old.append(k_average[index_average])#.insert(j, k_average[index_average])#.append(k_average[j])
                    gflops_old.append(gflops[j])
                    mtx_old.append(mtx[j])
                    k_histogram_old.append(k_histogram[j])
                    k_old.append(k[j])
            name_count+=1
            average_yes=0
            
print(name_count)#995

print(mtx_real[796])

length_k_standard=len(k_histogram_old)

'''
for i in range(1, listsize-1):
    #print(mtx[i])
    #print(mtx[i+1])
    if mtx[i] == mtx[i+1]:#在一个矩阵的数据集范围内
        in_mtx_counter[name_count]+=1#记录一个数据集内有几行数据，记录值为真实值减1
        #print(k[i])
        #print(k_average[i])
        #print("k[i] = %s, k_average[i] = %s" %(k[i], k_average[i]))
        if k[i] == k_average[i]:#k_average中的每一个元素含有换行符
            average_yes = 1
            index_average=i
            #print("average_yes = %d" %average_yes)
            #print("k = %d, k_average = %d ,average_yes = %d" %average_yes)
    else:
       # print("%s, %s" %(mtx[i-1], mtx[i]))
        if mtx[i-1] == mtx[i]:#本数据集的最后一行，在这个时候处理上一个数据集的结果写入
        #    print("average_yes = %d" %average_yes)
            if average_yes==1:
                for j in range(i-in_mtx_counter[name_count], i):
                    #print(j)
                    gflops_average.append(gflops[index_average])#.insert(j, gflops[index_average])#[j].append(gflops[j])
                    k_average_old.append(k_average[index_average])#.insert(j, k_average[index_average])#.append(k_average[j])
                    gflops_old.append(gflops[j])
                    mtx_old.append(mtx[j])
                    k_histogram_old.append(k_histogram[j])
                    k_old.append(k[j])
            name_count+=1
            average_yes=0
            
print(name_count)#995
length_k_standard=len(k_histogram_old)
'''

speedup=[0]*length_k_standard
for i in range(0, length_k_standard):
    speedup[i] = gflops_average[i]/gflops_old[i]


#print("length_standard=%d" %length_k_standard)

#print(in_mtx_counter)
df_train_speedup=DataFrame({'mtx':mtx_old, 'k':k_old, 'gflops_old':gflops_old, 'k_average_old':k_average_old, 'k_histogram_old':k_histogram_old, 'gflops_k_average':gflops_average, 'n_rows':n_r_standard, 'n_cols':n_c_standard, 'nnz_tot':nnz_t_standard, 'nnz_frac':nnz_f_standard, 'nnz_min':nnz_mi_standard, 'nnz_max':nnz_ma_standard, 'nnz_mu':nnz_mu_standard, 'nnz_sig':nnz_sig_standard, 'var':var_standard, 'speedup':speedup})

df_train_speedup.to_csv('feature_standard_average.csv')

print(df_train_speedup.iloc[0,:].size)
print(df_train_speedup.iloc[:,0].size)
print(df_train_speedup.iloc[0,1])
print(df_train_speedup.iloc[59556,15])
#print(df_train_speedup.iloc[:,1])
'''
for j in range(1,df_train_speedup.iloc[:,0].size):
    #if isinstance(df_train_speedup.iloc[j,1],str):
     #   print(df_train_speedup.iloc[j,1])
    tmp=math.log10(float(df_train_speedup.iloc[j,1]))#目标特征也需要归一化：preprocessing.standardscaler()
    print(tmp)
    df_train_speedup.iloc[j,1]=tmp
    print(j)
'''

for i in range(1,df_train_speedup.iloc[0,:].size):
    for j in range(0,df_train_speedup.iloc[:,0].size):
        if float(df_train_speedup.iloc[j,i]) != 0:
            #print(df_train_speedup.iloc[j,i])
            tmp=math.log10(float(df_train_speedup.iloc[j,i]))#目标特征也需要归一化：preprocessing.standardscaler()
            df_train_speedup.iloc[j,i]=tmp#特征值筛选：因子分析、PCA主成分析



df_train_speedup.to_csv('feature_standard_average_log_tmp.csv')



#print(listsize)
'''
with open("tmp_2.dat") as f2:#"hyb5_avx512_bestGFlops_3K.dat") as f1: 
    lines = f2.readlines()

for line in lines:
    if 'mtx' in line:
        st=line.split('mtx')[0].strip(string.punctuation)
        mtx_best.append(st)

        st=line.split(' ')[1]
        gflops_best.append(st)

        st=line.split(' ')[2]#.strip(string.punctuation)
        k_best.append(st)

#print(gflops_standard[0])
listsize_best=len(mtx_best)
'''
'''
in_mtx_counter=[0]*1100
index_average=0
average_yes=0
name_count=0
for i in range(0, listsize-1):
    if mtx[i] == mtx[i+1]:#在一个矩阵的数据集范围内
        in_mtx_counter[name_count]+=1#记录一个数据集内有几行数据，记录值为真实值减1
        if k[i] == k_average[i]:
            average_yes=1
            index_average=i
            #print(in_mtx_counter[name_count])
    elif mtx[i] == mtx[i-1]:#本数据集的最后一行，在这个时候处理上一个数据集的结果写入
        if average_yes==1:
            for j in range(i-in_mtx_counter[name_count]-1, i-1):
                gflops_standard.append(gflops[index_average])#.insert(j, gflops[index_average])#[j].append(gflops[j])
                k_standard.append(k_average[index_average])#.insert(j, k_average[index_average])#.append(k_average[j])
        elif average_yes==0:
            for j in range(i-in_mtx_counter[name_count]-1, i-1):
                gflops_standard.append(7777777)#.insert(j, 'none')#.append('none')
                k_standard.append(7777777)#.insert(j, 'none')#.append('none')
        name_count+=1
        average_yes=0
    else:
        k_standard.append(7777777)
        gflops_standard.append(7777777)


length_k_standard=len(k_standard)

print("length_standard=%d", length_k_standard)

print(in_mtx_counter)
'''

#df_train_speedup=DataFrame({'mtx':mtx, 'k_average':k_average, 'k_histogram':k_histogram, 'k':k, 'k_standard':k_standard, 'gflops_standard':gflops_standard})

#df_train_speedup.to_csv('feature_standard_average.csv')

'''
name_count=0
for i in range(0,listsize-1):
    #print(mtx[i])
    #print(mtx_best[name_count])
    if mtx_best[name_count] in mtx[i]:#mtx_best[name_count] != mtx[i]:#在一个矩阵的数据集范围内
        gflops_standard.append(gflops_best[name_count]) 
        k_standard.append(k_best[name_count])
        #in_mtx_counter.append()
        #for j in range(i, i+in_mtx_counter) 
        #in_mtx_counter=0
        
    elif name_count < listsize_best-1:
        #gflops_standard[i] = gflops_best[name_count]
        #k_standard[i] = k_best[name_count]
   # else:
        name_count+=1
        gflops_standard.append(gflops_best[name_count]) 
        k_standard.append(k_best[name_count])

print(len(mtx))
print(len(k_average))
print(len(gflops_standard))
print(len(k_standard))
'''
#




#print(gflops_best)





       # in_mtx_counter[name_count]++
       
