import string
import numpy as np 
from pandas import Series,DataFrame
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from pylab import *
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from sklearn import tree
from sklearn import cross_validation
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import cross_val_predict
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error
from sklearn.decomposition import PCA 
import collections

import subprocess
#subprocess.call(['dot', '-Tpdf', 'tree.dot', '-o' 'tree.pdf'])
import pydot
from scipy import stats as scistats


#frame=pd.read_csv('feature_standard_average_log2.csv', index_col=0)

frame=pd.read_csv('feature_standard_average.csv', index_col=0)

k_array=np.array(frame['k'])
sumsize=k_array.size

mtx_array=np.array(frame['mtx'], str).reshape(sumsize,1)
n_rows_array=np.array(frame['n_rows']).reshape(sumsize,1)
n_cols_array=np.array(frame['n_cols']).reshape(sumsize,1)
nnz_tot_array=np.array(frame['nnz_tot']).reshape(sumsize,1)
nnz_frac_array=np.array(frame['nnz_frac']).reshape(sumsize,1)
nnz_min_array=np.array(frame['nnz_min']).reshape(sumsize,1)
nnz_max_array=np.array(frame['nnz_max']).reshape(sumsize,1)
nnz_mu_array=np.array(frame['nnz_mu']).reshape(sumsize,1)#######
nnz_sig_array=np.array(frame['nnz_sig']).reshape(sumsize,1)
var_array=np.array(frame['var']).reshape(sumsize,1)
k_ave_array=np.array(frame['k_average_old']).reshape(sumsize,1)################
k_his_array=np.array(frame['k_histogram_old']).reshape(sumsize,1)###########
gflops_old_array=np.array(frame['gflops_old']).reshape(sumsize,1)
speedup=np.array(frame['speedup']).reshape(sumsize,1)###########

k_array = k_array.reshape((-1,1))

feature_scale = np.hstack((k_array, n_rows_array, n_cols_array, nnz_tot_array, nnz_frac_array, nnz_min_array, nnz_max_array, nnz_mu_array, nnz_sig_array, var_array, k_ave_array, k_his_array, gflops_old_array, speedup))

df_all = pd.DataFrame(feature_scale)
df_all.columns=['k', 'n_rows', 'n_cols', 'nnz_tot', 'nnz_frac', 'nnz_min','nnz_max','nnz_mu','nnz_sig','var','k_average','k_histogram', 'gflops_old','speedup']
df_all.insert(loc=0, column='mtx', value=mtx_array)

feature_train = np.hstack((k_array, n_rows_array, n_cols_array, nnz_tot_array, nnz_frac_array, nnz_min_array, nnz_max_array, nnz_mu_array, nnz_sig_array, var_array, k_ave_array, k_his_array, gflops_old_array))

pca=PCA(n_components=3)
reduced_feature_train=pca.fit_transform(feature_train)

df_reduced_feature=DataFrame(reduced_feature_train)
df_reduced_feature.insert(loc=0, column='mtx', value=mtx_array)
df_reduced_feature.insert(loc=4, column='speedup', value=speedup)
#df_reduced_feature_train.to_csv('reduced_feature_train.csv')

df_reduced_train=df_reduced_feature[0:47850]
df_reduced_train.to_csv('reduced_feature_train.csv')
df_reduced_test=df_reduced_feature[47851:]
df_reduced_test.to_csv('reduced_feature_test.csv')
'''
standard = StandardScaler()
feature_scale = standard.fit_transform(feature_scale)

df_scale_all = pd.DataFrame(feature_scale)
df_scale_all.columns=['k', 'n_rows', 'n_cols', 'nnz_tot', 'nnz_frac', 'nnz_min','nnz_max','nnz_mu','nnz_sig','var','k_average','k_histogram','speedup']
df_scale_all.insert(loc=0, column='mtx', value=mtx_array)
'''

'''
frame_train=df_all[0:47850]#t2em 从fullb:47851开始测试
frame_train.to_csv('feature_train.csv')
frame_test=df_all[47851:]
frame_test.to_csv('feature_test.csv')
'''

'''
frame_train=df_scale_all[0:47850]#t2em 从fullb:47851开始测试
frame_train.to_csv('feature_train_log_scale.csv')
frame_test=df_scale_all[47851:]
frame_test.to_csv('feature_test_log_scale.csv')
'''







