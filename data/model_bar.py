import matplotlib.pyplot as plt

x=['SVM','MLP','DT','SGD','KNC','GBR']

y=[0.9292, 0.8361, 0.79483, 0.79484, 0.786, 0.7927]

font1 = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   : 17,
}
plt.bar(x,y, color='indianred', alpha=1, width=0.5)
plt.ylim(0.75,0.95)
plt.xlabel('Regressor',font1)
plt.ylabel('Prediction Accuracy',font1)

plt.show()