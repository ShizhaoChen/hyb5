#-*- coding: utf-8 -*-
import pandas as pd
from pandas import Series,DataFrame
import seaborn as sns
import matplotlib.pyplot as plt 
from matplotlib.font_manager import FontProperties 
import subprocess
#subprocess.call(['dot', '-Tpdf', 'tree.dot', '-o' 'tree.pdf'])
import pydot
from scipy import stats as scistats
import string
import numpy as np 
 

font2 = {'family': 'Times New Roman',  
        'color':  'black',  
        'weight': 'normal',  
        'size': 12,  
        }  

def df():
    with open('k_on_perf.dat') as f: 
        lines = f.readlines()

    index=[]
    gf_data=[]
    for line in lines:
        if 'GFlops' in line:
            st=line.split(',')[0].split('=')[1]
            if st not in index:
                index.append(int(st))
                st=line.split('GFlops')[1][3:]
                gf_data.append(float(st))
    

        
    return index,gf_data


   
if __name__ == '__main__':
    x,y=df()


    plt.figure()  
    plt.plot(x,y,'o-',label='HYB')
    plt.plot([0,80],[6.07,6.07],c='r',label='ELL')
    plt.plot([0,80],[8.4,8.4],c='y',label='CSR')
    xx=45
    yy=8.7
    plt.text(xx, yy, r'$histogram$', fontdict=font2)
    plt.annotate('',xy=(35,8.53),xytext=(xx,yy),arrowprops=dict(arrowstyle="->",connectionstyle="arc3"))
    xx=40
    yy=7.0
    plt.text(xx, yy, r'$average$', fontdict=font2)
    plt.annotate('',xy=(48,7.63),xytext=(xx+10,yy+0.1),arrowprops=dict(arrowstyle="->",connectionstyle="arc3"))
    xx=60
    yy=5.7
    plt.text(xx, yy, r'$A$', fontdict=font2)
    plt.annotate('',xy=(65,5.53),xytext=(xx+2,yy),arrowprops=dict(arrowstyle="->",connectionstyle="arc3"))
    
    plt.ylabel('Gflops',size=12)
    plt.legend(loc='best')
    leg = plt.gca().get_legend()
    ltext  = leg.get_texts()
    plt.setp(ltext, fontsize=12)
    
    plt.subplots_adjust(bottom=0,top=0.6)

    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.subplots_adjust(bottom=0.3,top=0.8)    
    plt.savefig('K_perf.pdf',dpi=400,bbox_inches='tight')
    plt.show()
    '''
    plt.title('ft2000p_thread64(mtx orderd by nnz)')
    plt.ylabel('GFlops')
    #plt.xlabel('mtx_num')
    plt.legend(loc='upper left',ncol=1)
    plt.xlim([0,1000])
    plt.ylim([0,60])
    plt.savefig('scatter.pdf',dpi=400,bbox_inches='tight')
    '''


    
